// algoritmo del salto del caballo

#include<iostream>
using namespace std;

const int n = 10;
int tablero[n][n];

// desplazamiento del caballo

int d[8][2] = {{2,1},{1,2},{-1,2},{-2,1},{-2,-1},{-1,-2},{1,-2},{2,-1}};

void escribirtablero(){
	int i,j;
	
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			cout<<tablero[i][j]<<"|";
			
		}
		cout<<endl;
	}
	
	cout<<endl;

}



void saltocaballo(int i,int x,int y,bool &exito){
	int nx,ny;
	int k=0; //iterador para controlar los desplazamiento del caballo
	exito = false;
	
	do{
		k++;
		nx = x + d[k-1][0];
		ny = y + d[k-1][1];
		
		//verificacion si las coordenadas son correctas
		if((nx>=0) && (nx<n) && (ny>=0) && (ny<n) && (tablero[nx][ny]==0)){
			// guardamo movimientos
			tablero[nx][ny] = i;
			escribirtablero();// mostramos como el caballo se va moviendo
			
			if(i < n*n){
				saltocaballo(i+1,nx,ny,exito);
				
				if ( !exito){
					tablero[nx][ny] = 0;
				}
			}
			else{
				exito = true;
			}
		}
	}while((k<8) && !exito );
}

int main(){
	bool exito;
	int fila=1;
	int col=0;
	
	
	tablero[fila][col] = 1;
	
	saltocaballo(2,fila,col,exito);
	
	if(exito){//si exicto es verdadero
		cout<<"camino encontrado"<<endl;
		escribirtablero();
		
	}
	else{
		cout<<"camino no encontrado"<<endl;
	}
	return 0;
}
